# -*- coding: utf-8 -*-
"""
Description: This program generate .sql files (with meta info commented) from metabase questions
Author: LGDS Info team
"""

import argparse
import os
import re
import shutil
import unicodedata

from typing import List
from helpers.logging import get_logger
from tasks.metabase import get_all_cards, get_matching_field_id_name, translation_path

LOGGER = get_logger("sync_metabase")


def _get_metabase_card() -> list:
    """ Return all metabase cards """
    cards_info = get_all_cards()
    matching_field_id_name = get_matching_field_id_name()
    mb_cards_to_sync = []
    for card_info in cards_info:
        if "native" not in card_info["dataset_query"]:
            LOGGER.info(
                f"This card (id = {card_info['id']} is not written as a native query so it won't be processed"
            )
        else:
            if not card_info["archived"]:
                LOGGER.info(f"Retrieving card '{card_info['name']}' info")

                card_field_filters: List = []

                if "template-tags" in card_info["dataset_query"]["native"]:
                    card_filters = (
                        card_info["dataset_query"]["native"]["template-tags"] or {}
                    )

                    card_field_filters = [
                        f"{{{{ {filter_name} }}}} -> {matching_field_id_name[filter_info['dimension'][1]]} \n"
                        for filter_name, filter_info in card_filters.items()
                        if filter_info["type"] == "dimension"
                    ]

                collection_full_path = (
                    card_info["collection"]["location"]
                    + str(card_info["collection"]["id"])
                    if card_info["collection"]
                    else "/"
                )

                card_to_sync = {
                    "sql": card_info["dataset_query"]["native"]["query"],
                    "description": card_info["description"],
                    "card_name": card_info["name"],
                    "card_id": card_info["id"],
                    "card_field_filters": card_field_filters,
                    "collection_path": translation_path(collection_full_path),
                }

                mb_cards_to_sync.append(card_to_sync)
                LOGGER.info(
                    f"End of getting info from card '{card_info['name']}' (id = {card_info['id']})"
                )

    return mb_cards_to_sync


def _cleaning_reporting_folder(folder: str) -> None:
    """ Remove all file in a folder """

    LOGGER.info(f"Start cleaning reporting folder: {folder}")
    for files in os.listdir(folder):
        filepath = os.path.join(folder, files)
        LOGGER.info(f"Removing file : {filepath}")
        try:
            shutil.rmtree(filepath)
        except OSError:
            os.remove(filepath)


def _write_card_info(folder: str, cards_to_sync: list) -> None:
    """ write """

    for card_to_sync in cards_to_sync:

        card_name_clean = re.sub(
            r"[^a-zA-Z0-9\n\.]",
            " ",
            str(
                unicodedata.normalize("NFKD", card_to_sync["card_name"])
                .encode("ASCII", "ignore")
                .decode("utf-8")
            ),
        )
        folderpath = os.path.join(folder, card_to_sync["collection_path"])

        os.makedirs(folderpath, exist_ok=True)

        filepath = os.path.join(
            folderpath, f"{card_to_sync['card_id']}-{card_name_clean}.sql"
        )

        sql_file = open(filepath, "w")

        sql_file.write("/* \n -------- Description --------\n\n")
        sql_file.write((card_to_sync["description"] or "") + "\n\n")

        sql_file.write("\n -------- Filters translation -------- \n\n")

        for card_filter in card_to_sync["card_field_filters"]:
            sql_file.write(card_filter)

        sql_file.write("\n -------- SQL -------- */\n\n")
        sql_file.write(card_to_sync["sql"])

        sql_file.close()

        LOGGER.info(f"End writing file {filepath}")


if __name__ == "__main__":
    """
    Main function
    Args:
        folder (str): Destination folder name for .sql files
    Returns:
    """

    # Arg
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument(
        "--folder",
        help="Please enter a destination folder name for .sql files generated",
    )
    ARGS = PARSER.parse_args()

    # Get metabase cards info
    CARDS_TO_SYNC = _get_metabase_card()
    # Clean local folder
    if os.path.isdir(ARGS.folder):
        _cleaning_reporting_folder(ARGS.folder)
    # Write card
    _write_card_info(f"{ARGS.folder}", CARDS_TO_SYNC)
