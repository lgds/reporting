# -*- coding: utf-8 -*-
"""
Description: This module allow reading config file
Author: LGDS Info team
"""

import configparser
from pkg_resources import resource_string


CONFIG_FILENAME = "../.conf"


# PROJECT CONFIGURATION
CONFIG_CONTENT = resource_string(__name__, CONFIG_FILENAME)

CONFIG = configparser.ConfigParser()
CONFIG.read_string(CONFIG_CONTENT.decode("utf-8"))
