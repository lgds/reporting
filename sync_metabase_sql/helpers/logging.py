# -*- coding: utf-8 -*-
"""
Description: This module provides logging function
Author: LGDS Info team
"""

import logging


LOG_LEVEL = "INFO"


def get_logger(app_name: str = "export_metabase_sql") -> logging.Logger:
    """ Return a logger  """
    logging.basicConfig(format="%(asctime)s %(levelname)s:%(message)s", level=LOG_LEVEL)
    return logging.getLogger(app_name)
