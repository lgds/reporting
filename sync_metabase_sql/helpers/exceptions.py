# -*- coding: utf-8 -*-
"""
Description: This module contains our specific Python Helpers exceptions
Author: LGDS Info team
"""


class RequestException(Exception):
    """ Exception for request helper """


class MetabaseException(Exception):
    """ Exception for metabase helper """
