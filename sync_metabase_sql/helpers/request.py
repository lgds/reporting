# -*- coding: utf-8 -*-
"""
Description: This module provides high level requests functions
Author: LGDS Info team
"""

import json
import re
from typing import Any, Union, Dict, Optional

import requests
from helpers.logging import get_logger
from helpers.exceptions import RequestException
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


LOGGER = get_logger("request_helper")


def generate_request_session(
    credentials: Any = (),
    is_secure: bool = True,
    max_retries: int = 50,
    headers: Optional[Dict[str, str]] = None,
) -> requests.Session:
    """ Generate a session """

    adapter = "https://" if is_secure else "http://"

    session = requests.Session()
    if credentials != ():
        session.auth = credentials
    session.headers = headers  # type: ignore
    retry = Retry(
        total=max_retries,
        read=max_retries,
        connect=max_retries,
        backoff_factor=80,
        status_forcelist=(500, 502, 504),
    )
    session.mount(adapter, HTTPAdapter(max_retries=retry))

    return session


def call_endpoint(
    session: requests.Session,
    method: str,
    url: str,
    parameters: dict = None,
    data: Union[str, dict] = None,
) -> Any:
    """
    Call an endpoint
    Exceptions:
        RequestException when error occured during the endpoint call
    """

    if not re.match("^http[s]?://", url):
        raise RequestException("Url must start with 'https://' or 'http://'")

    try:
        result = session.request(method, url, params=parameters, data=data)
    except RequestException as request_exception:
        raise RequestException(
            f"Following error appeared while calling endpoint '{url}': {request_exception}"
        )

    if result.status_code not in (200, 202):
        error_msg = (
            f"Couldn't get request response due to http error code : {result.status_code} \n "
            f"Details: {result.text}"
        )
        raise RequestException(error_msg)

    return json.loads(result.text)


def simple_post_call(url: str, headers: dict = None, data: dict = None) -> dict:
    """ Execute a simple post call with request """
    response_token = requests.post(
        url,
        headers=headers,
        data=json.dumps(data),
    )

    if response_token.status_code != 200:
        error_msg = (
            f"Couldn't get request response due to http error code : {response_token.status_code} \n "
            f"Details: {response_token.text}"
        )
        raise RequestException(error_msg)

    return json.loads(response_token.text)
