# -*- coding: utf-8 -*-
"""
Description: This module provides low level metabase functions
Author: LGDS Info team
"""

from helpers.logging import get_logger
from helpers.config import CONFIG
from helpers.request import generate_request_session, simple_post_call
from requests import Session as request_session


LOGGER = get_logger("metabase_helper")


def _get_metabase_token(username: str, password: str, url_session: str) -> str:
    """ Generate a metabase access token and returns it """
    payload_get_metabase_token = {"username": username, "password": password}

    response_token = simple_post_call(
        url=url_session,
        headers={"Content-Type": "application/json"},
        data=payload_get_metabase_token,
    )

    return response_token["id"]


def _prepare_headers(token: str) -> dict:
    """ Return a formatted header for http requests """
    return {"X-Metabase-Session": token, "Content-Type": "application/json"}


def _generate_metabase_session() -> request_session:
    """ Generate a request session for metabase """
    token = _get_metabase_token(
        CONFIG.get("metabase_info", "username"),
        CONFIG.get("metabase_info", "password"),
        CONFIG.get("metabase_info", "url") + "/api/session/",
    )
    http_header = _prepare_headers(token)
    return generate_request_session(headers=http_header)


METABASE_SESSION = _generate_metabase_session()
