# -*- coding: utf-8 -*-
"""
Description: This module provides high level metabase functions
Author: LGDS Info team
"""
from typing import List, Dict, Any
import json
from helpers.config import CONFIG
from helpers.metabase import METABASE_SESSION
from helpers.exceptions import MetabaseException
from helpers.request import call_endpoint
from helpers.logging import get_logger

LOGGER = get_logger("task_metabase")
COLLECTION_COLOR = "#509EE3"


def _get_collection_translation() -> dict:
    """ Get all metabase collection except personal and return a dict like { collection_id : collection_name } """
    endpoint = f"{CONFIG.get('metabase_info', 'url')}/api/collection/"
    collections_metadata = call_endpoint(METABASE_SESSION, "get", endpoint)

    # Do not translation personal collection
    collection_translation = {
        str(collection["id"]): collection["name"]
        for collection in collections_metadata
        if collection["id"] == "root"
        or (collection["id"] != "root" and not collection["personal_owner_id"])
    }
    return collection_translation


COLLECTION_TRANSLATION = _get_collection_translation()


def get_all_cards() -> List:
    """ Return all cards payload """
    endpoint = f"{CONFIG.get('metabase_info', 'url')}/api/card/"
    return call_endpoint(METABASE_SESSION, "get", endpoint)


def get_matching_field_id_name() -> dict:
    """From metabase of all databases connected
    build a dict with field_id as key and table_name.field_name as value"""
    endpoint = f"{CONFIG.get('metabase_info', 'url')}/api/database"
    databases = call_endpoint(METABASE_SESSION, "get", endpoint)

    field_id_name_matching = {}


    for database in databases['data']:
        endpoint = f"{CONFIG.get('metabase_info', 'url')}/api/database/{database['id']}/metadata"
        database_metadata = call_endpoint(METABASE_SESSION, "get", endpoint)
        for table in database_metadata["tables"]:
            for field in table["fields"]:
                field_id_name_matching[field["id"]] = f"{table['name']}.{field['name']}"

    return field_id_name_matching


def translation_path(collection_path: str) -> str:
    """Translate a metabase collection path with ids to collection path with name
    ie 5/4/19 will be translated to name_collection_id_5/name_collection_id_4/name_collection_id_19"""
    collection_ids = list(filter(None, collection_path.split("/")))

    collections_path = [
        COLLECTION_TRANSLATION[collection_id]
        if collection_id in COLLECTION_TRANSLATION
        else "Collection-" + str(collection_id)
        for collection_id in collection_ids
    ]

    collection_path = "/".join(collections_path)

    return collection_path


def search_create_collection(
    collection_name: str,
    parent_collection_id: int = None,
) -> int:
    """ search a collection, if exists return directly its ID else create it """

    endpoint_search_collection = (
        f"{CONFIG.get('metabase_info', 'url')}/api/search?q={collection_name}"
    )
    response_collection_found = call_endpoint(
        METABASE_SESSION, "get", endpoint_search_collection
    )

    collection_found = [
        item
        for item in response_collection_found
        if item["model"] == "collection" and item["name"] == collection_name
    ]

    if len(collection_found) > 1:
        LOGGER.error(f"Too much collection corresponding to {collection_name}.")
        raise MetabaseException(
            f"Too much collection corresponding to {collection_name}."
        )

    if len(collection_found) == 1:
        clinic_collection_id = collection_found[0]["id"]
        LOGGER.info(
            f"Collection {collection_name} found (id = {clinic_collection_id})."
        )
    else:
        LOGGER.info(f"Collection {collection_name} not found.")
        data = {
            "name": collection_name,
            "parent_id": parent_collection_id,
            "color": COLLECTION_COLOR,
        }

        clinic_collection_id = call_endpoint(
            METABASE_SESSION,
            "post",
            url=f"{CONFIG.get('metabase_info', 'url')}/api/collection",
            data=json.dumps(data),
        )["id"]

    return clinic_collection_id


def search_create_question(
    card_name: str,
    collection_id: int,
    card_sql: str,
) -> int:
    """ Create a card from sql statement return id of the card """

    endpoint_search_card = (
        f"{CONFIG.get('metabase_info', 'url')}/api/search?q={card_name}"
    )
    response_card_found = call_endpoint(METABASE_SESSION, "get", endpoint_search_card)

    card_found = [
        item
        for item in response_card_found
        if item["model"] == "card"
        and item["name"] == card_name
        and item["collection_id"] == collection_id
    ]

    if len(card_found) > 1:
        LOGGER.error(f"Too much card corresponding to {card_name}.")
        raise MetabaseException(f"Too much card corresponding to {card_name}.")

    if len(card_found) == 1:
        LOGGER.info(f"Card '{card_name}' found and will be updated")
        card_id = card_found[0]["id"]

        endpoint_param = (
            "put",
            f"{CONFIG.get('metabase_info', 'url')}/api/card/{card_id}",
        )
    else:
        LOGGER.info(f"Card '{card_name}' not found. Will be created.")
        endpoint_param = (
            "post",
            f"{CONFIG.get('metabase_info', 'url')}/api/card",
        )

    payload_card: Dict[str, Any] = {}
    payload_card["result_metadata"] = []
    payload_card["collection_id"] = collection_id if collection_id != "root" else None
    payload_card["name"] = card_name
    payload_card["dataset_query"] = {
        "database": int(CONFIG.get("metabase_info", "database_id")),
        "type": "native",
    }
    payload_card["dataset_query"]["native"] = {"query": card_sql}
    payload_card["display"] = "table"
    payload_card["visualization_settings"] = {}

    response_card_info = call_endpoint(
        METABASE_SESSION,
        *endpoint_param,
        data=json.dumps(payload_card),
    )
    returned_card_id = response_card_info["id"]
    LOGGER.info(
        f"Card '{card_name}' {'updated' if len(card_found) == 1 else 'created'} (id = {returned_card_id})."
    )

    return returned_card_id
