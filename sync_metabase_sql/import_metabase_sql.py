# -*- coding: utf-8 -*-
"""
Description: This program aims to generate metabase cards from sql files
Author: LGDS Info team
"""

import argparse
import os
import sys

from helpers.logging import get_logger
from tasks.metabase import search_create_collection, search_create_question

LOGGER = get_logger("sync_metabase")

if __name__ == "__main__":
    """
    Main function
    Args:
        folder (str): source folder name for .sql files to import
    Returns:
    """

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument(
        "--folder",
        help="Source folder name for .sql files to import",
    )
    ARGS = PARSER.parse_args()

    # Check if the folder exists and if it not empty
    if not os.path.isdir(ARGS.folder):
        LOGGER.info("Folder doesn't exists")
        sys.exit()

    if len(os.listdir(ARGS.folder)) == 0:
        LOGGER.info("Folder exist but is empty")
        sys.exit()

    # Search or create if the folder already exists in metabase
    COLLECTTION_NAME = ARGS.folder.split("/")[len(ARGS.folder.split("/")) - 1]
    COLECTION_ID = search_create_collection(COLLECTTION_NAME)

    for root, subdirs, files in os.walk(ARGS.folder):

        for file in files:

            # Only read .sql files
            if len(file) > 4 and file[-4:] == ".sql":
                filepath = os.path.join(root, file)

                with open(filepath, "rb") as f:
                    card_sql = f.read()

                    card_name = str(file.split(".")[0])

                    search_create_question(
                        card_name, COLECTION_ID, card_sql.decode("utf-8")
                    )
