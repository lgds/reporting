# Metabase utils

This directory aims to help any [Metabase](https://www.metabase.com/) user to share their questions by providing functions to **export** and **import** them.

_Only native metabase questions are affected._

- `export_metabase_sql.py`: export every native metabase questions in .sql files
- `import_metabase_sql.py`: import .sql files contained in a specified folder into metabase

If you want to participate to the code you can follow the [Developpement](#developpement) part or directly [execute the script](#run-scripts)

## Developpement

Use following tools for proper python installation:
 - PyEnv
 - Poetry

### Dev environment installation

**1. Install [pyenv](https://github.com/pyenv/pyenv#homebrew-on-macos) (python version manager)**

```bash
brew install pyenv # On mac os
```

**2. Install 3.9.1 with pyenv**

```bash
pyenv install 3.9.1
pyenv global 3.9.1
pyenv shell 3.9.1

#Test
python3.9 -V
# Python 3.9.1
```

**3. Install [poetry](https://github.com/python-poetry/poetry) (python package-management system)**

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

**4. Add poetry and pyenv to ~/.zshrc**

```bash
# Add poetry to your shell
export PATH="$HOME/.poetry/bin:$PATH"

# Add pyenv initializer to shell startup script.
echo -e '\nif command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi' >> ~/.zshrc

# Reload your profile.
source ~/.zshrc

# Test
poetry -V
# Poetry version 1.1.4
```

**5. Add folder path to your python path**

```bash
export PYTHONPATH=$PYTHONPATH:[Path]/reporting/sync_metabase_sql
```

**6. Install repo's dependencies**

```bash
# Install dependencies
poetry install # This will create a .venv folder inside your repo folder with all the packages inside
```

The python path for your interpreter will be as follow

```bash
[Path]/sync_metabase_sql/.venv/bin/python3.9
```


## Configuration

You need to configure the `.conf` file in order to allow the program to have access to your metabase.

 ```bash
cp .conf.local .conf
```

The file looks like:

```
[metabase_info]
username : metabase_db_username
password : metabase_db_pwd
url : https://metabase.my_coop.fr
database_id : 2 # Only needed for the import function
```

You can find the database_id in the end of the url of your database info (databases are listed here in `https://[your_metabase].fr/admin/databases`)

Fill the username, password and url.


## Run scripts

 ```bash
python export_metabase_sql.py --folder=../your_destination_folder_name
```

 ```bash
python import_metabase_sql.py --folder=../your_source_folder_name
```
