/* 
 -------- Description --------

[categorie,  date perte]


 -------- Filters translation -------- 

{{ categ_name }} -> product_category.name 
{{ loss_creation_date }} -> stock_picking.create_date 

 -------- SQL -------- */

SELECT 
product_category.name AS "Cat."
, SUM(sm.product_qty*sm.price_unit) as "Valeur perte"
FROM stock_picking
JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
JOIN stock_move sm ON sm.picking_id = stock_picking.id
JOIN product_product pp ON pp.id = sm.product_id
JOIN product_template pt ON pt.id = pp.product_tmpl_id
JOIN product_category ON product_category.id = pt.categ_id
WHERE 
  spt.name = 'Pertes'
  AND stock_picking.create_date >= '2021-02-09'
  [[ AND {{ categ_name }} ]]
  [[ AND {{ loss_creation_date }} ]]
GROUP BY 1
ORDER BY 1 DESC