/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH stat_perte AS (
    SELECT  DATE_TRUNC('month', stock_picking.create_date) as period
    , SUM(CASE WHEN spt.name = 'Pertes' THEN sm.product_qty*sm.price_unit ELSE 0 END) as total_perte
    FROM stock_picking
    JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
    JOIN stock_move sm ON sm.picking_id = stock_picking.id
    WHERE 
      spt.name IN ('Pertes')
      AND stock_picking.create_date >= '2021-02-17'
      AND stock_picking.create_date >= DATE_TRUNC('month',now()) - interval '12 month'
    GROUP BY 1
), stat_vente AS (
    SELECT  DATE_TRUNC('month', pos_order.create_date) as period
    , SUM(pos_order.amount_untaxed) as total_vente
    FROM pos_order
    WHERE  pos_order.create_date >= '2021-02-17'
      AND pos_order.create_date >= DATE_TRUNC('month',now()) - interval '12 month'
    GROUP BY 1
)
SELECT stat_vente.period, CASE WHEN total_vente != 0 THEN total_perte/total_vente::decimal(10,2)*100 ELSE 0 END AS tx_perte, total_perte, total_vente
FROM stat_vente
JOIN stat_perte ON stat_perte.period = stat_vente.period