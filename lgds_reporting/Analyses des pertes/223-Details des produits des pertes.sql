/* 
 -------- Description --------

[ Date perte, stock picking id, categorie produit]


 -------- Filters translation -------- 

{{ id_stock_picking }} -> stock_picking.id 
{{ categ_name }} -> product_category.name 
{{ loss_creation_date }} -> stock_picking.create_date 

 -------- SQL -------- */

WITH tmp as (SELECT 
            RIGHT(LEFT(stock_picking.name,28),-9) AS "Info perte"
            , product_category.name AS "Cat."
            , pp.name_template
            , pt.base_price
            , sm.product_qty
            , sm.product_qty*sm.price_unit as total_price
            FROM stock_picking
            JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
            JOIN stock_move sm ON sm.picking_id = stock_picking.id
            JOIN product_product pp ON pp.id = sm.product_id
            JOIN product_template pt ON pt.id = pp.product_tmpl_id
            JOIN product_category ON product_category.id = pt.categ_id
            WHERE 
              spt.name = 'Pertes'
              AND stock_picking.create_date >= '2021-02-09'
              [[ AND {{ categ_name }} ]]
              [[ AND {{ loss_creation_date }} ]]
              [[ AND {{ id_stock_picking }} ]]
)
SELECT "Info perte"
, "Cat."
, name_template
, base_price as "Prix Article"
, product_qty as "Nb article"
, total_price as "Valeur perte"
FROM tmp 
UNION
SELECT '00- Total','All','-', null, sum(product_qty), sum(total_price)
FROM tmp
ORDER BY 1,2,3 DESC