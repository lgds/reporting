/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ categ_name }} -> product_category.name 
{{ loss_creation_date }} -> stock_picking.create_date 

 -------- SQL -------- */

WITH stat_vente AS (
    SELECT  DATE_TRUNC({{time_granularity}}, stock_picking.create_date) as period
    , SUM(CASE WHEN spt.name = 'Pertes' THEN sm.product_qty*sm.price_unit ELSE 0 END) as total_perte
    , SUM(CASE WHEN spt.name = 'PoS Orders' THEN sm.product_qty*sm.price_unit ELSE 0 END) as total_vente
    FROM stock_picking
    JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
    JOIN stock_move sm ON sm.picking_id = stock_picking.id
    JOIN product_product pp ON pp.id = sm.product_id
    JOIN product_template pt ON pt.id = pp.product_tmpl_id
    JOIN product_category ON product_category.id = pt.categ_id
    WHERE 
      spt.name IN ('Pertes', 'PoS Orders')
      AND stock_picking.create_date >= '2021-02-17'
      [[ AND {{ categ_name }} ]]
      [[ AND {{ loss_creation_date }} ]]
    GROUP BY 1
)
SELECT period, CASE WHEN total_vente != 0 THEN total_perte/total_vente::decimal(10,2)*100 ELSE 0 END AS tx_perte
FROM stat_vente