/* 
 -------- Description --------

[ Date, Product Id ]


 -------- Filters translation -------- 

{{ loss_creation_date }} -> stock_picking.create_date 

 -------- SQL -------- */

SELECT 
RIGHT(LEFT(stock_picking.name,28),-9) AS "Info perte",
sm.product_qty as "Qt."
FROM stock_picking
JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
JOIN stock_move sm ON sm.picking_id = stock_picking.id
JOIN product_product pp ON pp.id = sm.product_id
WHERE 
  spt.name = 'Pertes'
  AND stock_picking.create_date >= '2021-02-09'
  AND pp.product_tmpl_id = {{ product_id }}
  [[ AND {{ loss_creation_date }} ]]
ORDER BY 1 DESC
  
