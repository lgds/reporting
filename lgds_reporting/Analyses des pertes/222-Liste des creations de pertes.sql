/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ loss_creation_date }} -> stock_picking.create_date 

 -------- SQL -------- */

WITH tmp AS (SELECT 
stock_picking.id
, RIGHT(stock_picking.name,-9) AS "Info perte"
, stock_picking.inventory_value AS "Valeur perte"
FROM stock_picking
JOIN stock_picking_type spt ON stock_picking.picking_type_id = spt.id
WHERE 
  spt.name = 'Pertes'
  AND stock_picking.create_date >= '2021-02-09'
  [[AND {{ loss_creation_date }}]]
ORDER BY 1 DESC 
LIMIT 10)
SELECT * FROM tmp
UNION 
SELECT NULL, 'Total', SUM("Valeur perte") FROM tmp
ORDER BY 2 DESC