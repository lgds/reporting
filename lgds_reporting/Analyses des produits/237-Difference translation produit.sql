/* 
 -------- Description --------

[Fournisseur, categorie, produit]


 -------- Filters translation -------- 

{{ category_name }} -> product_category.name 

 -------- SQL -------- */

SELECT 
product_category.name as "Categorie"
, res_partner.name AS "Fournisseur"
, product_product.name_template AS "nom produit"
, ir_translation.value AS "traduction"
FROM product_product 
JOIN product_template ON product_template.id = product_product.product_tmpl_id
JOIN product_category ON product_category.id = product_template.categ_id
JOIN ir_translation ON ir_translation.res_id = product_product.product_tmpl_id AND ir_translation.name = 'product.template,name' 
JOIN product_supplierinfo ON product_supplierinfo.product_tmpl_id = product_product.product_tmpl_id
JOIN res_partner ON res_partner.id = product_supplierinfo.name
WHERE product_product.name_template != ir_translation.value
AND product_product.active
[[AND {{ category_name }} ]]
[[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
[[AND translate (lower(res_partner.name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') =   translate (lower({{ supplier_name }}), 'çñaàâeéêèioô', 'cnaaaeeeeioo')   ]]
ORDER BY 1,2,3,4