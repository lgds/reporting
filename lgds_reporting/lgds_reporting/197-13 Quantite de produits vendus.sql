/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[Periode temps, liste produit, nom produit]


 -------- Filters translation -------- 

{{ product_name_list }} -> product_product.name_template 
{{ date_vente }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH dim_date AS(
    SELECT TO_CHAR(datum, 'YYYY-MM-dd') AS id,
           datum AS date_actual,
          TO_CHAR(datum + (1 - EXTRACT(ISODOW FROM datum))::INT, 'IW - [dd/MM/YY -') || TO_CHAR(datum + (7 - EXTRACT(ISODOW FROM datum))::INT, ' dd/MM/YY]') as week_label
    FROM (SELECT '2020-01-01'::DATE + SEQUENCE.DAY AS datum
          FROM GENERATE_SERIES(0, 700) AS SEQUENCE (DAY)
          GROUP BY SEQUENCE.DAY) DQ
)
SELECT name_template
,CASE lower({{ time_granularity }})
            WHEN 'day' THEN  TO_CHAR(pos_order_line.create_date,'YYYY-MM-dd') 
            WHEN 'week' THEN week_label 
            WHEN 'dayweek' THEN CASE TO_CHAR(pos_order_line.create_date,'dy')  WHEN 'mon' THEN 'lun' WHEN 'tue' THEN 'mar' WHEN 'wed' THEN 'mer' WHEN 'thu' THEN 'jeu' WHEN 'fri' THEN 'ven' WHEN 'sat' THEN 'sam' WHEN 'sun' THEN 'dim' END
            WHEN 'month' THEN TO_CHAR(pos_order_line.create_date,'YYYY-MM') 
            
        END AS order_date
,CASE lower({{ time_granularity }})
            WHEN 'day' THEN  TO_CHAR(pos_order_line.create_date,'S-IW') 
        END AS order_date_precision
, SUM(qty) as quantity_sold
FROM product_product
JOIN pos_order_line ON pos_order_line.product_id = product_product.id
JOIN dim_date ON dim_date.id = TO_CHAR(pos_order_line.create_date,'YYYY-MM-dd')
WHERE pos_order_line.create_date >= now() - interval '12 months'
AND {{ date_vente }}
[[ AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  '%' || {{ product_name }} || '%' ]]
[[ AND {{ product_name_list }}]]
[[ AND product_product.product_tmpl_id = {{ product_id }}]]
GROUP BY 1,2,3
ORDER BY 3,1