/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[Product Name]


 -------- Filters translation -------- 

{{ product_name_list }} -> product_product.name_template 
{{ category_name }} -> product_category.name 

 -------- SQL -------- */

WITH dim_date AS(
    SELECT TO_CHAR(datum, 'YYYY-MM-dd') AS id,
           datum AS date_actual,
          TO_CHAR(datum + (1 - EXTRACT(ISODOW FROM datum))::INT, 'IW - [dd/MM/YY -') || TO_CHAR(datum + (7 - EXTRACT(ISODOW FROM datum))::INT, ' dd/MM/YY]') as week_label
    FROM (SELECT '2020-01-01'::DATE + SEQUENCE.DAY AS datum
          FROM GENERATE_SERIES(0, 700) AS SEQUENCE (DAY)
          GROUP BY SEQUENCE.DAY) DQ
)
SELECT   product_template.id 
, product_category.name as "Categorie"
, res_partner.name AS "Fournisseur"
, coalesce(ir_translation.value, product_product.name_template) AS "Nom produit"
, product_code AS "Ref fournisseur"
, product_product.barcode
, default_code      AS "Ref interne"
, product_supplierinfo.package_qty AS "Cond."
, product_template.weight_net
, product_template.volume
, product_template.list_price AS "Prix de vente"
, ROUND(RIGHT(MAX(TO_CHAR(stock_quant.create_date, 'YYYY-MM-dd HH:MI:SS') || cost),-19)::decimal(10,2),2) AS cout
, SUM(stock_quant.qty) AS stock_dispo
FROM product_product
LEFT JOIN stock_quant ON product_id = product_product.id AND location_id = 12
JOIN product_template ON product_template.id = product_product.product_tmpl_id
JOIN product_category ON product_category.id = product_template.categ_id
JOIN product_supplierinfo ON product_supplierinfo.product_tmpl_id = product_template.id
JOIN res_partner ON res_partner.id = product_supplierinfo.name
LEFT JOIN ir_translation ON ir_translation.res_id = product_product.product_tmpl_id AND ir_translation.name = 'product.template,name' 
WHERE product_product.active
AND (product_supplierinfo.date_end IS NULL OR product_supplierinfo.date_end > now())
[[AND {{ product_name_list }} ]]
[[AND {{ category_name }} ]]
[[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
[[AND translate (lower(res_partner.name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') =   translate (lower({{ supplier_name }}), 'çñaàâeéêèioô', 'cnaaaeeeeioo')   ]]
GROUP BY 1,2,3,4,5,6,7,8,9,10,11
ORDER BY "Nom produit"
LIMIT 15 OFFSET {{ num_page }}*15