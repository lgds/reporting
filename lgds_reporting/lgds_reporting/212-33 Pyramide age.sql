/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH age_stat AS( 
    SELECT 
        sex,  
        EXTRACT(YEAR FROM AGE(birthdate::date)) as age,
        count(*) as cpt_partner
    FROM res_partner 
    WHERE NOT (supplier or is_company)
    AND sex IN ('f','m')
    AND active
    GROUP BY 1,2
    ORDER BY 1
)
SELECT 
    sex,
    CASE 
        WHEN age < 30 THEN '<30' 
        WHEN age BETWEEN 30 AND 40 THEN '30-40' 
        WHEN age BETWEEN 40 AND 50 THEN '40-50' 
        WHEN age BETWEEN 50 AND 60 THEN '50-60' 
    ELSE '60+' END AS age_range,
    sum(cpt_partner)
FROM age_stat
GROUP BY 1,2
ORDER BY 2,1