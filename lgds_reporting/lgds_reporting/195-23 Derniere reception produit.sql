/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[Nom produit]


 -------- Filters translation -------- 

{{ product_name }} -> product_product.name_template 

 -------- SQL -------- */

WITH last_receipt AS (
    SELECT  origin 
    ,product_qty
    , price_unit
    , TO_CHAR(stock_move.date, 'dd/MM/YYYY') as date
    ,  ROW_NUMBER() OVER( ORDER BY stock_move.date DESC) as ranking
    FROM product_product 
    JOIN stock_move ON stock_move.product_id = product_product.id 
    JOIN stock_picking_type ON stock_picking_type.id = stock_move.picking_type_id
    WHERE product_product.product_tmpl_id = {{ product_id }}
    [[ {{ product_name }}]]
    AND stock_picking_type.name = 'Receipts'
    AND stock_move.state != 'cancel'
 )
SELECT origin AS "Ref commande"
, product_qty  AS "Qty"
, price_unit AS "Prix"
, date
FROM last_receipt
WHERE ranking <= 5
ORDER BY date desc