/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH tbl_col_num AS (
    SELECT  
    t.table_name as tableName,
    count(*) as nb_columns
    FROM information_schema.tables t
    LEFT JOIN information_schema.columns c on c.table_schema = t.table_schema
              and c.table_name = t.table_name
    WHERE t.table_schema not in ('information_schema', 'pg_catalog')
     [[AND t.table_name LIKE '%' || {{ table_name }} || '%' ]] 
    GROUP BY 1
)
SELECT
  pgClass.relname   AS tableName,
  pgClass.reltuples AS rowCount,
  nb_columns
FROM
  pg_class pgClass
LEFT JOIN
  pg_namespace pgNamespace ON (pgNamespace.oid = pgClass.relnamespace)
LEFT JOIN tbl_col_num ON pgClass.relname = tbl_col_num.tableName
WHERE
  pgNamespace.nspname NOT IN ('pg_catalog', 'information_schema') AND
  pgClass.relkind='r'
  [[ AND pgClass.relname  LIKE '%' || {{ table_name }} || '%' ]] 
ORDER BY rowcount DESC