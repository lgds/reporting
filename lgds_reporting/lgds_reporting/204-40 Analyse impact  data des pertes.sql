/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT stock_picking.name
, source_location.name as src_location
, dest_location.name as dest_location
, stock_quant.*
FROM stock_picking
JOIN stock_picking_type ON stock_picking_type.id = stock_picking.picking_type_id
JOIN stock_location source_location ON default_location_src_id = source_location.id
JOIN stock_location dest_location ON default_location_dest_id = dest_location.id
LEFT JOIN pos_order ON pos_order.picking_id = stock_picking.id -- no order linked
JOIN stock_move ON stock_move.picking_id = stock_picking.id
JOIN stock_quant_move_rel ON stock_quant_move_rel.move_id = stock_move.id
JOIN stock_quant ON stock_quant_move_rel.quant_id = stock_quant.id
WHERE stock_picking.id = 13324