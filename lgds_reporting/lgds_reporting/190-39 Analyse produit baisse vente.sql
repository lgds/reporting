/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ product_name }} -> product_product.name_template 
{{ category_name }} -> product_category.name 
{{ date_vente }} -> pos_order_line.create_date 
{{ product_name_list }} -> product_product.name_template 

 -------- SQL -------- */

WITH product_stock AS (
    SELECT product_id, SUM(stock_quant.qty) AS stock_dispo
    FROM product_product
    JOIN stock_quant ON product_id = product_product.id AND location_id = 12
    WHERE 1=1 
    [[AND {{ product_name }}]]
    GROUP BY 1
),
nb_jour AS (
    SELECT  COUNT(DISTINCT CASE WHEN {{ date_vente }} THEN TO_CHAR(pos_order_line.create_date,'YYYY-MM-dd') ELSE NULL END) AS nb_jour
    FROM pos_order_line
    WHERE {{ date_vente }}
)
SELECT product_category.name AS "Nom catégorie"
, product_product.product_tmpl_id AS "ID produit"
, product_product.name_template AS "Nom produit"
, nb_jour
, stock_dispo
, COUNT(distinct pos_order_line.order_id) as "Nb vente"
, COUNT(distinct pos_order_line.order_id)/nb_jour::decimal(10,2) as "Nb vente moyenne par jour"
, SUM(qty) as "Nb quantité vendue"
, SUM(qty)/nb_jour::decimal(10,2) as "Nb quantité vendue moyenne par jour"
, COUNT(distinct pos_order.partner_id) as "Nb acheteur"
, COUNT(distinct pos_order.partner_id)/nb_jour::decimal(10,2) as "Nb acheteur moyenne par jour"
FROM product_product
JOIN product_template ON product_product.product_tmpl_id = product_template.id 
JOIN product_category ON product_category.id = product_template.categ_id
LEFT JOIN pos_order_line ON pos_order_line.product_id = product_product.id
LEFT JOIN pos_order ON pos_order_line.order_id = pos_order.id
JOIN product_stock ON product_stock.product_id = product_product.id
JOIN nb_jour ON 1=1
WHERE 1=1
AND product_product.active
[[AND {{ date_vente }} ]]
[[AND {{ product_name_list }}]]
[[AND {{ category_name }}]]
[[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name_like }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
GROUP BY 1,2,3,4,5
HAVING 1=1
[[AND COUNT(distinct pos_order.partner_id) <= {{ max_nb_acheteur }} ]]
[[AND COUNT(distinct pos_order_line.order_id) > {{ max_nb_vente }} ]]
[[AND SUM(qty) > {{ max_qty_vendue }}]]
ORDER BY 1,3