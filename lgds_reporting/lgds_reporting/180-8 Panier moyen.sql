/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[Date vente, periode vente]


 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH order_ammount AS (
    SELECT TO_CHAR(create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) AS period
    , order_id
    , SUM(qty*price_unit) AS turnover
    , count(distinct product_id) as nb_product
    FROM pos_order_line
    WHERE {{ order_date }}
    GROUP BY 1,2 
)
SELECT period, avg(turnover) avg_basket,avg(nb_product) avg_nb_product, count(distinct order_id) as nb_order
FROM order_ammount
GROUP BY 1
ORDER BY 1