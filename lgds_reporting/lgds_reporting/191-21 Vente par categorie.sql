/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 
{{ category }} -> product_category.name 

 -------- SQL -------- */

SELECT product_category.name
, SUM( pos_order_line.qty*pos_order_line.price_unit) as "chiffre d'affaire"
FROM pos_order_line
JOIN pos_order ON pos_order.id = pos_order_line.order_id
JOIN product_product ON product_product.id = pos_order_line.product_id 
JOIN product_template ON product_template.id = product_product.product_tmpl_id 
JOIN product_category ON product_category.id = product_template.categ_id 
WHERE {{ order_date }}
[[AND {{ category }}]]
GROUP BY 1
ORDER BY 2 DESC