/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[Date Commande, fournisseur, categorie, liste produit, produit like]


 -------- Filters translation -------- 

{{ product_name_list }} -> product_product.name_template 
{{ category_name }} -> product_category.name 
{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

SELECT  
product_category.name as "Categorie"
, res_partner.name AS "Fournisseur"
, coalesce(ir_translation.value, product_product.name_template) AS "Nom produit"
, SUM(pos_order_line.qty) AS "Nb article"
, COUNT(DISTINCT pos_order_line.order_id ) AS "Comptage"
, SUM(pos_order_line.qty*pos_order_line.price_unit) AS "Chiffre d'affaire" 
, product_template.id
FROM product_product
JOIN product_template ON product_template.id = product_product.product_tmpl_id
JOIN product_category ON product_category.id = product_template.categ_id
JOIN product_supplierinfo ON product_supplierinfo.product_tmpl_id = product_template.id
JOIN res_partner ON res_partner.id = product_supplierinfo.name
LEFT JOIN pos_order_line ON product_product.id = pos_order_line.product_id
LEFT JOIN ir_translation ON ir_translation.res_id = product_product.product_tmpl_id AND ir_translation.name = 'product.template,name' 
WHERE 1=1 
AND (product_supplierinfo.date_end IS NULL OR product_supplierinfo.date_end > now())
[[AND {{ product_name_list }} ]]
[[AND {{ order_date }} ]]
[[AND {{ category_name }} ]]
[[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
[[AND translate (lower(res_partner.name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE   translate (lower('%' ||{{ supplier_name }} || '%'), 'çñaàâeéêèioô', 'cnaaaeeeeioo')   ]]
GROUP BY 1,2,3,7
ORDER BY 1,2