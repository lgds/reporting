/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ date_creneau }} -> shift_registration.date_begin 
{{ etat_creneau }} -> shift_registration.state 

 -------- SQL -------- */

SELECT TO_CHAR(shift_registration.date_begin, 'YYYY-MM') As "Date créneau"
, shift_registration.state
, count(*) AS nb_enregistrement_creneau
FROM shift_ticket
JOIN shift_registration ON shift_registration.shift_ticket_id = shift_ticket.id
JOIN res_partner ON shift_registration.partner_id = res_partner.id
JOIN shift_shift ON shift_shift.id = shift_ticket.shift_id
WHERE  1=1
--AND shift_registration.date_begin = '2020-12-18 15:15'
AND shift_registration.state not in  ('cancel','waiting')
AND shift_registration.date_begin BETWEEN date_trunc('month',now()) - interval '12 month' AND date_trunc('month',now()) + interval '1 month'
[[AND {{ date_creneau }}]]
[[AND {{ etat_creneau }}]]
[[AND barcode_base = {{ barcode_base }}]]   
GROUP BY 1,2