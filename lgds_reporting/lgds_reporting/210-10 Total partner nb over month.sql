/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH nb_partner_per_day AS (
    SELECT  
        DATE_TRUNC({{time_granularity}}, create_date) as date_inscription, 
        COUNT(*) as nb_partner
    FROM res_partner
    WHERE create_date >= '2020-09-01'
    AND not supplier
    AND active
    GROUP BY 1
)
SELECT 
    date_inscription,
    SUM(nb_partner) OVER (ORDER BY date_inscription) AS cum_amt
FROM   nb_partner_per_day
ORDER  BY 1;
