/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT 
    sex,  
    DATE_TRUNC({{time_granularity}}, create_date) as date,
    count(*) as cpt_partner
FROM res_partner 
WHERE NOT (supplier OR is_company)
AND sex IN ('f','m')
AND active
AND create_date >= '2020-10-01' 
AND create_date >= DATE_TRUNC('month', now()) - interval '12 month' 
GROUP BY 1, 2
ORDER BY 2