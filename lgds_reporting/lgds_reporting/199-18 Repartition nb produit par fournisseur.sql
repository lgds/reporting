/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------

[]


 -------- Filters translation -------- 


 -------- SQL -------- */

    SELECT res_partner.name, count(distinct product_product.id) as nb_product
    FROM res_partner
    JOIN product_supplierinfo ON res_partner.id = product_supplierinfo.name 
    JOIN product_template ON product_template.id = product_supplierinfo.product_tmpl_id
    JOIN product_product ON product_product.product_tmpl_id = product_template.id 
    WHERE product_product.active
    GROUP BY 1
    ORDER BY 2 DESC