/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ product_name_list }} -> product_product.name_template 
{{ category_name }} -> product_category.name 
{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

SELECT  
'Total' AS total
, SUM(pos_order_line.qty*pos_order_line.price_unit) AS "Chiffre d'affaire" 
, SUM(pos_order_line.qty) AS "Quantité d'article"
, COUNT(DISTINCT pos_order_line.id ) AS "Comptage"
FROM product_product
JOIN product_template ON product_template.id = product_product.product_tmpl_id
JOIN product_category ON product_category.id = product_template.categ_id
LEFT JOIN pos_order_line ON product_product.id = pos_order_line.product_id
WHERE 1=1 
[[AND {{ product_name_list }} ]]
[[AND {{ order_date }} ]]
[[AND {{ category_name }} ]]
[[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
ORDER BY 1