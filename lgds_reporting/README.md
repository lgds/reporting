# LGDS - reporting metabase questions

Ce dossier contient toutes les questions Metabase que nous utilisons au supermarché [Les Grains de Sel](https://gitlab.com/lgds) à Paris XIIIème.

Tous les fichiers `*.sql` ont été exportés grâce au script présent dans [../sync_metabase_sql/](./../sync_metabase_sql/).
