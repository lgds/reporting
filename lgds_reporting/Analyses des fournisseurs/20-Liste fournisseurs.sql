/* 
 -------- Description --------

[Fournisseur]


 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT DISTINCT name
FROM res_partner
WHERE (supplier OR is_company)
[[ AND translate (lower(name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  '%' || translate (lower({{ supplier_name }}), 'çñaàâeéêèioô', 'cnaaaeeeeioo')  || '%' ]]

ORDER BY 1