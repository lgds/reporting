/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ category_name }} -> product_category.name 
{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH stock AS (
    SELECT product_id, sum(qty) as stock_qty
    FROM stock_quant
    WHERE location_id = 12
    GROUP BY 1
)
SELECT  
DATE_TRUNC({{ time_granularity }}, pos_order_line.create_date) AS order_date
, res_partner.name AS "Fournisseur"
, SUM(pos_order_line.qty*pos_order_line.price_unit) AS "Chiffre d'affaire" 
FROM product_product
JOIN product_template ON product_template.id = product_product.product_tmpl_id
JOIN product_category ON product_category.id = product_template.categ_id
JOIN product_supplierinfo ON product_supplierinfo.product_tmpl_id = product_template.id
JOIN res_partner ON res_partner.id = product_supplierinfo.name
LEFT JOIN stock ON stock.product_id = product_product.id
LEFT JOIN pos_order_line ON product_product.id = pos_order_line.product_id
LEFT JOIN ir_translation ON ir_translation.res_id = product_product.product_tmpl_id AND ir_translation.name = 'product.template,name' 
WHERE 1=1 
AND (product_supplierinfo.date_end IS NULL OR product_supplierinfo.date_end > now())
[[AND {{ order_date }} ]]
[[AND {{ category_name }} ]]
[[AND translate (lower(res_partner.name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') ~ translate (lower({{ supplier_name }}), 'çñaàâeéêèioô', 'cnaaaeeeeioo')   ]]
GROUP BY 1,2
ORDER BY 1,2