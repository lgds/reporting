/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ product_name }} -> product_product.name_template 
{{ category_name }} -> product_category.name 

 -------- SQL -------- */

WITH product_stat AS (
    WITH product_stock AS (
        SELECT product_id, SUM(stock_quant.qty) AS stock_dispo
        FROM product_product
        JOIN stock_quant ON product_id = product_product.id AND location_id = 12
        WHERE 1=1 
        [[AND {{ product_name }}]]
        GROUP BY 1
    )
    
    SELECT res_partner.name  AS "fournisseur"
    , product_category.name AS "categorie"
    , product_product.name_template AS "produit"
    , stock_dispo
    , TO_CHAR(now(),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END)
    , TO_CHAR(now()- cast('1 ' ||  {{ time_period }} as INTERVAL),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END)
    , TO_CHAR(now()- cast('2 ' ||  {{ time_period }} as INTERVAL),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) 
    , SUM(DISTINCT CASE WHEN TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) = TO_CHAR(now()- cast('3 ' ||  {{ time_period }} as INTERVAL),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) THEN pos_order_line.qty ELSE 0 END) AS "Période - 3"
    , SUM(DISTINCT CASE WHEN TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) = TO_CHAR(now()- cast('2 ' ||  {{ time_period }} as INTERVAL),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) THEN pos_order_line.qty ELSE 0 END) AS "Période - 2"
    , SUM(DISTINCT CASE WHEN TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) = TO_CHAR(now()- cast('1 ' ||  {{ time_period }} as INTERVAL),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) THEN pos_order_line.qty ELSE 0 END) AS "Période - 1"
    , SUM(DISTINCT CASE WHEN TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) = TO_CHAR(now(),'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) THEN pos_order_line.qty ELSE 0 END) AS "Période actuelle"
    FROM res_partner
    JOIN product_supplierinfo ON res_partner.id = product_supplierinfo.name 
    JOIN product_template ON product_template.id = product_supplierinfo.product_tmpl_id
    JOIN product_product ON product_product.product_tmpl_id = product_template.id 
    JOIN product_category ON product_category.id = product_template.categ_id
    LEFT JOIN pos_order_line ON pos_order_line.product_id = product_product.id
    JOIN product_stock ON product_stock.product_id = product_product.id
    WHERE 1=1
    AND pos_order_line.create_date >= DATE_TRUNC('week',now())- interval '3 weeks'
    [[AND res_partner.name = {{ supplier_name }}]]
    [[AND {{ product_name }}]]
    [[AND {{ category_name }}]]
    GROUP BY 1,2,3,4,5,6
    ORDER BY stock_dispo DESC
)
SELECT fournisseur, categorie, produit, ("Période - 3" + "Période - 2" + "Période - 1")/3 as avg_last_week, "Période - 1", "Période actuelle", stock_dispo
FROM product_stat
WHERE stock_dispo - ("Période - 3" + "Période - 2" + "Période - 1")/3 <= 0