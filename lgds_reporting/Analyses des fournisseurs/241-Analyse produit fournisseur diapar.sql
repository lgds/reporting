/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT res_partner.name, product_name, product_tmpl_id, product_code, is_product_active
FROM product_supplierinfo
JOIN res_partner ON product_supplierinfo.name = res_partner.id
WHERE lower(res_partner.name) LIKE '%diapar%'
AND length(product_code) != 6
[[AND product_code LIKE '%' || {{ product_code }} || '%']]
[[AND translate (lower(product_name), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
ORDER BY 5,1,2
