/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT res_partner.name AS "Fournisseur"
, coalesce(ir_translation.value, product_supplierinfo.product_name) AS "Nom produit"
, TO_CHAR(pos_order_line.create_date, 'YYYY-MM') as period
, SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover
FROM vracoop.product_product
JOIN vracoop.product_supplierinfo ON product_supplierinfo.product_tmpl_id = product_product.product_tmpl_id
JOIN vracoop.res_partner ON res_partner.id = product_supplierinfo.name
LEFT JOIN vracoop.ir_translation ON ir_translation.res_id = product_product.product_tmpl_id AND ir_translation.name = 'product.template,name' 
JOIN vracoop.pos_order_line ON pos_order_line.product_id = product_product.id
WHERE product_product.active
AND res_partner.id = 28
GROUP BY 1,2,3 
ORDER BY 1,2,3