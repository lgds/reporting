/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT 
    *,
    zip || ' ' || TRIM(BOTH FROM INITCAP(LOWER(city))) as Ville,
    EXTRACT(YEAR from AGE(birthdate::timestamp)) as age,
    CASE 
        WHEN EXTRACT(YEAR from AGE(birthdate::timestamp)) < 30 THEN '<30' 
        WHEN EXTRACT(YEAR from AGE(birthdate::timestamp)) BETWEEN 30 AND 39 THEN '30-39' 
        WHEN EXTRACT(YEAR from AGE(birthdate::timestamp)) BETWEEN 40 AND 49 THEN '40-49' 
        WHEN EXTRACT(YEAR from AGE(birthdate::timestamp)) BETWEEN 50 AND 59 THEN '50-59'
    ELSE '60+' END AS age_range
FROM res_partner
WHERE create_date >= '2020-09-01'
  AND is_member
  AND NOT (supplier OR is_company)