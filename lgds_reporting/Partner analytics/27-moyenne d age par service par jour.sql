/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH data AS (
    SELECT  LEFT(TO_CHAR(now(), 'YYYY-MM-dd'),4)::int - LEFT(birthdate,4)::int as age
    , TO_CHAR(shift_registration.date_begin + interval '1 hour','dy') as jour_semaine
    , TO_CHAR(shift_registration.date_begin + interval '1 hour','HH24:MI') as horaire
    , res_partner.id
    FROM shift_ticket
    JOIN shift_registration ON shift_registration.shift_ticket_id = shift_ticket.id
    JOIN res_partner ON shift_registration.partner_id = res_partner.id
    JOIN shift_shift ON shift_shift.id = shift_ticket.shift_id
    WHERE  1=1
    --AND shift_registration.date_begin = '2020-12-05 12:30'
    AND shift_registration.state not in  ('cancel','waiting')
)
SELECT horaire
, AVG( CASE WHEN jour_semaine ='mon' THEN age ELSE NULL END ) as lundi
, AVG( CASE WHEN jour_semaine ='tue' THEN age ELSE NULL END ) as mardi
, AVG( CASE WHEN jour_semaine ='wed' THEN age ELSE NULL END ) as mercredi
, AVG( CASE WHEN jour_semaine ='thu' THEN age ELSE NULL END ) as jeudi
, AVG( CASE WHEN jour_semaine ='fri' THEN age ELSE NULL END ) as vendredi
, AVG( CASE WHEN jour_semaine ='sat' THEN age ELSE NULL END ) as samedi
, AVG( CASE WHEN jour_semaine ='sun' THEN age ELSE NULL END ) as dimanche
/*, COUNT( DISTINCT CASE WHEN jour_semaine ='mon' THEN id ELSE NULL END ) as "lundi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='tue' THEN id ELSE NULL END ) as "mardi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='wed' THEN id ELSE NULL END ) as "mercredi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='thu' THEN id ELSE NULL END ) as "jeudi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='fri' THEN id ELSE NULL END ) as "vendredi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='sat' THEN id ELSE NULL END ) as "samedi(nb coop)"
, COUNT( DISTINCT CASE WHEN jour_semaine ='sun' THEN id ELSE NULL END ) as "dimanche(nb coop)"*/
FROM data
WHERE horaire BETWEEN '06:15' and '19:00'
GROUP BY 1
ORDER BY 1
