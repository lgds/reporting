/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ date_partner }} -> res_partner.create_date 

 -------- SQL -------- */

SELECT  
    city, 
    COUNT(*) as nb_partner
FROM res_partner
WHERE create_date >= '2020-09-01'
AND not supplier
AND active
AND sex != 'o'
AND NOT cooperative_state IN ('not_concerned', 'unsubscribed')
[[ AND {{date_partner}}]]
GROUP BY 1
ORDER BY 2 DESC

