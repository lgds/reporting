/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ date_partner }} -> res_partner.create_date 

 -------- SQL -------- */

SELECT 
    sex,  
    DATE_TRUNC({{time_granularity}}, create_date) as date,
    count(*) as cpt_partner
FROM res_partner 
WHERE NOT (supplier OR is_company)
AND sex IN ('f','m')
AND active
AND {{ date_partner }}
AND create_date>= '2020-10-01'

GROUP BY 1, 2
ORDER BY 2