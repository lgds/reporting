/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ date_partner }} -> res_partner.create_date 

 -------- SQL -------- */

WITH nb_partner_per_day AS (
    SELECT  
        DATE_TRUNC({{time_granularity}}, create_date) as date_inscription, 
        sex,
        COUNT(*) as nb_partner
    FROM res_partner
    WHERE create_date>= '2020-09-01'
    AND not supplier
    AND active
    AND sex != 'o'
    AND NOT cooperative_state IN ('not_concerned', 'unsubscribed')
    [[ AND {{date_partner}}]]
    GROUP BY 1,2
    ORDER BY 1,2
)
SELECT 
    date_inscription,
    sex,
    SUM(nb_partner) OVER (PARTITION BY sex ORDER BY date_inscription) AS cum_amt
FROM   nb_partner_per_day
ORDER  BY 1,2;
