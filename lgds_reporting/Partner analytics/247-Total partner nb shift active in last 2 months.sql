/* 
 -------- Description --------

Nombre total de coopérateurs qui ont fait au moins un service dans les 2 derniers mois, groupés par date d'inscription


 -------- Filters translation -------- 

{{ date_partner }} -> res_partner.create_date 

 -------- SQL -------- */

WITH nb_partner_per_day AS (
    SELECT  
        DATE_TRUNC({{time_granularity}}, res_partner.create_date) as date_inscription, 
        COUNT(DISTINCT res_partner.id) as nb_partner
    FROM res_partner
    INNER JOIN shift_registration AS sr ON sr.partner_id = res_partner.id
    WHERE res_partner.create_date >= '2020-09-01'
    AND not supplier
    AND active
    AND NOT cooperative_state IN ('not_concerned', 'unsubscribed')
    AND sr.state = 'done' AND sr.date_begin >= NOW() - interval '2 month'
    [[ AND {{date_partner}}]]
    GROUP BY 1
)
SELECT 
    date_inscription,
    SUM(nb_partner) OVER (ORDER BY date_inscription) AS cum_amt
FROM   nb_partner_per_day
ORDER  BY 1;
