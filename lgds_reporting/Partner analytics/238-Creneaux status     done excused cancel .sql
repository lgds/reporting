/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ date_begin }} -> shift_registration.date_begin 

 -------- SQL -------- */

SELECT state, date_begin, template_created, partner_id, create_date, shift_type, name
FROM shift_registration
WHERE state NOT IN ('done', 'excused', 'cancel')
AND {{ date_begin }}