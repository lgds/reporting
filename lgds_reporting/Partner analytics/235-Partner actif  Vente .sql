/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order.create_date 

 -------- SQL -------- */

SELECT DATE_TRUNC('month',create_date) as mois
, count(distinct partner_id ) as nb_partner_actif
FROM pos_order 
WHERE {{ order_date }}
AND state = 'done'
GROUP BY 1
ORDER BY 1