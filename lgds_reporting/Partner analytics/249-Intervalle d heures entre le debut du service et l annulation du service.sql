/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT round(extract(epoch from "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date")/3600) AS "cancellation_interval", count(*) AS "count"
FROM "public"."shift_registration"
WHERE 
  "public"."shift_registration"."state" = 'cancel' 
  AND CAST("public"."shift_registration"."date_begin" AS date) > CAST('2021-09-01' AS date) 
  AND date_trunc('hour', "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date") < interval '48 hours'
  AND date_trunc('hour', "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date") >= interval '0 hours'
GROUP BY round(extract(epoch from "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date")/3600)
ORDER BY round(extract(epoch from "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date")/3600) ASC