/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH age_stat AS( 
    SELECT 
        sex,  
        EXTRACT(YEAR FROM AGE(birthdate::date)) as age,
        count(*) as cpt_partner
    FROM res_partner
    WHERE create_date >= '2020-09-01'
        AND sex IN ('m', 'f')
        AND NOT (supplier OR is_company)
        AND cooperative_state IS NOT null
        AND NOT cooperative_state IN ('unsubscribed')
    GROUP BY 1,2
    ORDER BY 1
)
SELECT 
    sex,
    CASE 
        WHEN age < 30 THEN '<30' 
        WHEN age BETWEEN 30 AND 40 THEN '30-40' 
        WHEN age BETWEEN 40 AND 50 THEN '40-50' 
        WHEN age BETWEEN 50 AND 60 THEN '50-60' 
    ELSE '60+' END AS age_range,
    sum(cpt_partner)
FROM age_stat
GROUP BY 1,2
ORDER BY 2,1