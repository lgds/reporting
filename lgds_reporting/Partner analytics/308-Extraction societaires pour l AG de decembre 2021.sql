/* 
 -------- Description --------

À la demande de Béatrice pour l'envoyer à une société de service qui va fournir une solution en ligne pour organiser l'AG.

(cf https://www.nuag.fr/)


 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT
    SPLIT_PART(name, ', ', 2) AS prénom,
    SPLIT_PART(name, ', ', 1) AS nom,
    email,
    CASE function
    WHEN 'SA' THEN 'salariés'
    WHEN 'SO' THEN 'soutiens'
    WHEN 'P' THEN 'producteurs'
    ELSE 'coopérateurs'
    END AS collège
FROM
    res_partner
WHERE
    is_member