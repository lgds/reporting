/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH profesional_partner AS (
    SELECT  TO_CHAR(create_date, 'YYYY-MM') as create_date
    , COUNT(DISTINCT CASE WHEN is_company then id ELSE NULL END) AS nb_company
    , COUNT(DISTINCT CASE WHEN supplier then id ELSE NULL END) AS nb_supplier
    , COUNT(*) as nb_all
    FROM res_partner
    WHERE is_company or supplier
    GROUP BY 1
)
SELECT create_date
, SUM(nb_company) OVER(ORDER BY create_date) as nb_company
, SUM(nb_supplier) OVER(ORDER BY create_date) as nb_supplier
, SUM(nb_all) OVER(ORDER BY create_date) as nb_all
FROM profesional_partner
ORDER BY 1
