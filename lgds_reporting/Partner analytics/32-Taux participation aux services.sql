/* 
 -------- Description --------

Compte seulement les coops de - de 60 ans, et ceux qui ont au minimum -2 points (pour éviter de compter ceux qui ont des points négatifs depuis longtemps)


 -------- Filters translation -------- 

{{ date_creneau }} -> shift_registration.date_begin 
{{ etat_creneau }} -> shift_registration.state 

 -------- SQL -------- */

SELECT TO_CHAR(shift_registration.date_begin, 'YYYY-MM') As "Date créneau"
, shift_registration.state
, count(*) AS nb_enregistrement_creneau
FROM shift_ticket
JOIN shift_registration ON shift_registration.shift_ticket_id = shift_ticket.id
JOIN res_partner ON shift_registration.partner_id = res_partner.id
JOIN shift_shift ON shift_shift.id = shift_ticket.shift_id
WHERE res_partner.final_standard_point > -3 AND res_partner.final_ftop_point > -3 
AND EXTRACT(YEAR FROM AGE(res_partner.birthdate::timestamp)) < 60
AND shift_registration.state not in  ('open', 'draft', 'cancel','waiting')
[[AND {{ date_creneau }}]]
[[AND {{ etat_creneau }}]]
[[AND barcode_base = {{ barcode_base }}]]   
GROUP BY 1,2