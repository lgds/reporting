/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH source AS (
    SELECT 
        id
    FROM res_partner
    WHERE create_date >= '2020-09-01'
      AND is_member
      AND NOT (supplier
        OR is_company)
)
SELECT 
    "source"."id" AS "id", 
    sum("Account Invoice"."amount_total") AS total_parts_sociales, 
    CASE WHEN sum("Account Invoice"."amount_total") < 100 THEN '< 100'
         WHEN sum("Account Invoice"."amount_total") = 100 THEN '= 100'
         WHEN sum("Account Invoice"."amount_total") > 100 THEN '> 100'
    END AS fenetre_parts_sociales
FROM "source"
LEFT JOIN "public"."account_invoice" "Account Invoice" ON "source"."id" = "Account Invoice"."partner_id" 
LEFT JOIN "public"."capital_fundraising_category" "Capital Fundraising Category - Fundraising Category" ON "Account Invoice"."fundraising_category_id" = "Capital Fundraising Category - Fundraising Category"."id" 
WHERE ((lower("Capital Fundraising Category - Fundraising Category"."name") like '%parts%') AND "Account Invoice"."state" = 'paid')
GROUP BY "source"."id"
ORDER BY "source"."id" ASC