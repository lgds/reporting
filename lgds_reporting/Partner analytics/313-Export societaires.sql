/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT 
    split_part("source"."name", ', ', 1) AS "Nom", 
    split_part("source"."name", ', ', 2) AS "Prénom",
    "source"."sex" AS "Genre",
    CASE 
        WHEN "source"."function" ILIKE '%SO%'  THEN 'Soutiens'
        WHEN "source"."function" ILIKE '%SA%' THEN 'Salariés'
        WHEN "source"."function" ILIKE '%P%' THEN 'Producteurs'
        ELSE 'Coopérateurs'
    END AS "Nom du Collège", 
    CASE 
        WHEN "source"."function" ILIKE '%SO%'  THEN 0.1
        WHEN "source"."function" ILIKE '%SA%' THEN 0.3
        WHEN "source"."function" ILIKE '%P%' THEN 0.2
        ELSE 0.3
    END AS "poids de vote",
    "source"."email" AS "email"
FROM {{#253}} "source"
WHERE "source"."function" IS NULL OR NOT "source"."function" ILIKE '%D%'
ORDER BY "source"."function" ASC
LIMIT 1048575