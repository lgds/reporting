/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ shift_date }} -> shift_registration.date_begin 

 -------- SQL -------- */

SELECT DATE_TRUNC('month',date_begin) as mois
, count(distinct partner_id ) as nb_partner_actif
FROM shift_registration 
WHERE {{ shift_date }}
AND state = 'done'
GROUP BY 1
ORDER BY 1