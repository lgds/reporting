/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT 
    (COUNT(*) FILTER (WHERE date_trunc('hour', "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date") < interval '24 hours') )::NUMERIC
    /
    COUNT(*)::NUMERIC AS pourcentage_annulation_moins_24h
FROM "public"."shift_registration"
WHERE 
  "public"."shift_registration"."state" = 'cancel' 
  AND CAST("public"."shift_registration"."date_begin" AS date) > CAST('2020-09-01' AS date)
  AND date_trunc('hour', "public"."shift_registration"."date_begin" - "public"."shift_registration"."write_date") >= interval '0 hours'