/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

select t.table_schema,
       t.table_name,
       c.column_name, 
       c.data_type
from information_schema.tables t
inner join information_schema.columns c on c.table_name = t.table_name 
                                and c.table_schema = t.table_schema
where t.table_schema not in ('information_schema', 'pg_catalog')
      [[and t.table_name = {{ table_name }} ]]
      [[and c.column_name LIKE '%' || {{ column_name }} || '%']]
order by t.table_schema;