/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH category_translation AS (
    SELECT 
    CASE 
    WHEN product_category.name IN ('Animaux','Bazar','Entretien 5.5%','Entretien 20%','TVA10','Librairie','Presse') THEN 'Bazar'
    WHEN product_category.name IN ('Bien-être','Bien-être 20%','Hygiène 20%','Hygiène','Enfance 20%') THEN 'Hygiène'
    WHEN product_category.name IN ('Boucherie','Poissonnerie') THEN 'Viande/Poisson'
    WHEN product_category.name IN ('Cave','Vins','Vrac Cave') THEN 'Cave'
    WHEN product_category.name IN ('Épicerie Sucrée','Épicerie Sucrée 20%','Enfance') THEN 'Epicerie Sucrée'
    WHEN product_category.name IN ('Frais','Oeufs') THEN 'Frais'
    WHEN product_category.name IN ('Vrac Liquide Entretien','Vrac Entretien','Vrac Liquide Hygiène') THEN 'Vrac Maison'
    WHEN product_category.name IN ('Vrac Salés','Vrac Liquide Cuisine') THEN 'Vrac Salée'
    WHEN product_category.name IN ('Vrac Sucré','Vrac Sucré 20%') THEN 'Vrac Sucré'
    WHEN product_category.name IN ('F&L') THEN 'Fruit et légume'
    ELSE product_category.name
    END as mother_category_name
    , product_category.id AS child_categ_id
    FROM product_category
    WHERE product_category.name NOT IN ('Vrac Reconditionné','Consigne','Archive 5.5','Préco','Vrac Frais','viins','Boxe 5.5','Archive 20%','Archive 20','All','Vrac Liquide Boisson')
)
, supplier_translation AS (
    SELECT  
        CASE 
        WHEN res_partner.name IN ('Biovive Rungis','Desailly','di spé ré Rungis','Huillion','Dynamis Epicerie','Maison bio sain Rungis','Dynamis Frais','Dynamis Surgelé','Dynamis Vrac') THEN 'Min / marché Rungis'
        WHEN res_partner.name IN ('Agidra','Agidra Ep Salée','Agidra Ep Sucrée// Boulang','Agidra Vrac') THEN 'Agidra'
        WHEN res_partner.name IN ('Andines','Andines Vrac', '') THEN 'Andines'
        WHEN res_partner.name IN ('Aurore Market','Aurore Market Vrac') THEN 'Agidra'
        WHEN res_partner.name IN ('Bio Alize F&L','Dynamis F&L','Mil Perche F&L','Coop Bio F&L') THEN 'Fruits et Légumes'
        WHEN res_partner.name IN ('Boissons Vivantes', 'Cocomiette') THEN 'Bière'
        WHEN res_partner.name IN ('Bryo', 'Bryo Vrac') THEN 'Bryo'
        WHEN res_partner.name IN ('Depeyrot','Agnès Lanchon','Caroline Gayet','Ma Green planète','Librairie Jonas') THEN 'Librairie'
        WHEN res_partner.name IN ('Diapar','Diapar Frais','Diapar Surgelé') THEN 'Diapar'
        WHEN res_partner.name IN ('Ecodis', 'Ecodis Vrac') THEN 'Ecodis'
        WHEN res_partner.name IN ('Ethiquable', 'Ethiquable Vrac') THEN 'Ethiquable'
        WHEN res_partner.name IN ('Ferme Hélicicole', 'Poisson Hélicicole') THEN 'Ferme Hélicicole'
        WHEN res_partner.name IN ('Léa Nature', 'Léa Vrac') THEN 'Léa Nature'
        WHEN res_partner.name IN ('Moulin des Moines', 'Moulin des Moines Vrac') THEN 'Moulin des Moines'
        WHEN res_partner.name IN ('Remy Biwand','Stephane Gardette Beaujolais',E'SCEA le Dime - Clos de l\'abbaye _400coops','GFA Jonnerys','Domaine Mortier Boisard','Bergerarc Grand Roc _ 400coop','Neveux-Vesselle-Champagne','Morin Domaine de la rodaie _ 400coop','Mirabeau Cocagne','Maison Grand Monteil','Maison Passot Beaujolais','Cantalauze','Cave des Vins de Sancerre','Domaine De Rocheville_400coops','Domaine Des Coeuriot _ 400coops','Domaine Lerys _ 400coop','Château Cajus','Château Souché','Clos Rocailleux') THEN 'Vin'
        WHEN res_partner.name IN ('Scoop Epices Reconditionné', 'Scoop Epice Vrac', 'Scoop Epices') THEN 'Scoop Epices'
        WHEN res_partner.name IN ('Terra Libra', 'Terra Libra Vrac') THEN 'Terra Libra'
        WHEN res_partner.name IN ('Vita Amb','Vita Surgelé','Vita Frais','Vita Vrac') THEN 'Vita Frais'
        ELSE res_partner.name
        END as mother_partner_name
        , res_partner.id AS child_partner_id
    FROM res_partner
    WHERE id IN (SELECT distinct name FROM product_supplierinfo)
)
SELECT supplier_translation.mother_partner_name as supplier_name
, category_translation.mother_category_name as category_name
, SUM( pos_order_line.qty*pos_order_line.price_unit) as "chiffre d'affaire"
FROM supplier_translation
JOIN product_supplierinfo ON supplier_translation.child_partner_id = product_supplierinfo.name 
JOIN product_template ON product_template.id = product_supplierinfo.product_tmpl_id
JOIN product_product ON product_product.product_tmpl_id = product_template.id
JOIN category_translation ON category_translation.child_categ_id = product_template.categ_id 
JOIN pos_order_line ON product_product.id = pos_order_line.product_id 
WHERE product_product.active
GROUP BY 1,2
ORDER BY 2 DESC