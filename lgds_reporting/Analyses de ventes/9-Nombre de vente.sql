/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 
{{ category }} -> product_category.name 

 -------- SQL -------- */

SELECT TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) AS period
, count(distinct partner_id) as nb_cooperateur
FROM pos_order_line
JOIN pos_order ON pos_order.id = pos_order_line.order_id
JOIN product_product ON product_product.id = pos_order_line.product_id 
JOIN product_template ON product_template.id = product_product.product_tmpl_id 
JOIN product_category ON product_category.id = product_template.categ_id 
WHERE {{ order_date }}
[[AND {{ category }}]]
GROUP BY 1