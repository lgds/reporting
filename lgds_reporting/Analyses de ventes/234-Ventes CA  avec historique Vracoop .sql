/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 
{{ categ_name }} -> product_category.name 
{{ product_name }} -> product_product.name_template 

 -------- SQL -------- */

WITH tmp as (
    SELECT  
        DATE_TRUNC({{ time_granularity }}, pos_order_line.create_date) AS order_date,
        SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover,
        SUM(qty) as quantity_ordered
    FROM pos_order_line
    JOIN product_product ON product_product.id = pos_order_line.product_id
    JOIN product_template pt ON product_product.product_tmpl_id= pt.id
    JOIN product_category on product_category.id = pt.categ_id
    WHERE {{ order_date }}
    [[AND {{ categ_name }} ]]
    [[AND {{ product_name }} ]]
    GROUP BY 1
    
    UNION
    
    SELECT  
        DATE_TRUNC({{ time_granularity }}, pos_order_line.create_date) AS order_date,
        SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover,
        SUM(qty) as quantity_ordered
    FROM vracoop.pos_order_line
    JOIN vracoop.product_product ON product_product.id = pos_order_line.product_id
    JOIN vracoop.product_template pt ON product_product.product_tmpl_id= pt.id
    JOIN vracoop.product_category on product_category.id = pt.categ_id
    WHERE {{ order_date }}
    [[AND {{ categ_name }} ]]
    [[AND {{ product_name }} ]]
    GROUP BY 1 
    ORDER BY 1
)
SELECT order_date, SUM(turnover) as turnover, sum(quantity_ordered) AS quantity_ordered
FROM tmp
GROUP BY 1