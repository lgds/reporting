/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ product_name }} -> product_product.name_template 
{{ category_name }} -> product_category.name 
{{ date_vente }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH stat_product AS (
    SELECT  res_partner.name  AS "Nom fournisseur"
    , product_category.name AS "Nom catégorie"
    , product_product.name_template AS "Nom produit"
    , TO_CHAR(pos_order_line.create_date, 'IW') as num_week
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 1 THEN pos_order_line.order_id ELSE NULL END) AS nb_lun
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 2 THEN pos_order_line.order_id ELSE NULL END) AS nb_mar
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 3 THEN pos_order_line.order_id ELSE NULL END) AS nb_mer
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 4 THEN pos_order_line.order_id ELSE NULL END) AS nb_jeu
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 5 THEN pos_order_line.order_id ELSE NULL END) AS nb_ven
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 6 THEN pos_order_line.order_id ELSE NULL END) AS nb_sat
    , COUNT(DISTINCT CASE WHEN extract(dow FROM pos_order_line.create_date) = 0 THEN pos_order_line.order_id ELSE NULL END) AS nb_dim
    FROM res_partner
    JOIN product_supplierinfo ON res_partner.id = product_supplierinfo.name 
    JOIN product_template ON product_template.id = product_supplierinfo.product_tmpl_id
    JOIN product_product ON product_product.product_tmpl_id = product_template.id 
    JOIN product_category ON product_category.id = product_template.categ_id
    LEFT JOIN pos_order_line ON pos_order_line.product_id = product_product.id
    WHERE 1=1
    AND pos_order_line.create_date BETWEEN CURRENT_DATE + CAST(-6 - extract(dow FROM CURRENT_DATE) AS INT) % 7 - cast({{ nb_week }} || ' week' as INTERVAL)  AND CURRENT_DATE + CAST(-6 - extract(dow FROM CURRENT_DATE) AS INT) % 7
    [[AND res_partner.name = {{ supplier_name }}]]
    [[AND {{ product_name }}]]
    [[AND {{ category_name }}]]
    [[AND {{ date_vente }}]]
    [[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name_like }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
    GROUP BY 1,2,3,4
)
SELECT "Nom fournisseur","Nom catégorie", "Nom produit"
, AVG(nb_lun) AS "lun"
, AVG(nb_mar) AS "mar"
, AVG(nb_mer) AS "mer"
, AVG(nb_jeu) AS "jeu"
, AVG(nb_ven) AS "ven"
, AVG(nb_sat) AS "sat"
, AVG(nb_dim) AS "dim"
FROM stat_product
GROUP BY 1,2,3
ORDER BY 1,2,3