/* 
 -------- Description --------

[Liste produit, produit like, fournisseur, categ, date vente]


 -------- Filters translation -------- 

{{ product_name_list }} -> product_product.name_template 
{{ category_name }} -> product_category.name 
{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH stat AS (SELECT  
    product_category.name as "Categorie"
    , SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover
    , SUM(pos_order_line.qty) AS nb_article
    , COUNT(DISTINCT pos_order_line.id ) AS "Comptage"
    FROM product_product
    JOIN product_template ON product_template.id = product_product.product_tmpl_id
    JOIN product_category ON product_category.id = product_template.categ_id
    LEFT JOIN pos_order_line ON product_product.id = pos_order_line.product_id
    WHERE 1=1 
    [[AND {{ product_name_list }} ]]
    [[AND {{ order_date }} ]]
    [[AND {{ category_name }} ]]
    [[AND translate (lower(name_template), 'çñaàâeéêèioô', 'cnaaaeeeeioo') LIKE  translate (lower('%' || {{ product_name }}) || '%', 'çñaàâeéêèioô', 'cnaaaeeeeioo')  ]]
    GROUP BY 1
    ORDER BY 1
)
SELECT "Categorie"
, turnover AS "Chiffre d'affaire" 
,nb_article AS "Quantité d'article"
,"Comptage"
FROM stat
UNION 
SELECT '0-Total', SUM(turnover), SUM(nb_article), SUM("Comptage") FROM stat
ORDER BY 1