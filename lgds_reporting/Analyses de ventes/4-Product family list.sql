/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ name_cat }} -> pos_category.name 

 -------- SQL -------- */

SELECT name 
FROM pos_category 
WHERE id IN (SELECT parent_id FROM pos_category WHERE parent_id IS NOT NULL)
AND {{ name_cat }}