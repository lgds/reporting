/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT  
    TO_CHAR(pos_order_line.create_date, 'YYYY') AS year_date,
    TO_CHAR(pos_order_line.create_date,'MM-Month') AS order_date,
    SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover,
    SUM(qty) as quantity_ordered
FROM pos_order_line
JOIN product_product ON product_product.id = pos_order_line.product_id
JOIN product_template pt ON product_product.product_tmpl_id= pt.id
JOIN product_category on product_category.id = pt.categ_id
WHERE pos_order_line.create_date < DATE_TRUNC('month',now())
GROUP BY 1,2

UNION

SELECT  
    TO_CHAR(pos_order_line.create_date, 'YYYY') AS year_date,
    TO_CHAR(pos_order_line.create_date,'MM-Month') AS order_date,
    SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover,
    SUM(qty) as quantity_ordered
FROM vracoop.pos_order_line
JOIN vracoop.product_product ON product_product.id = pos_order_line.product_id
JOIN vracoop.product_template pt ON product_product.product_tmpl_id= pt.id
JOIN vracoop.product_category on product_category.id = pt.categ_id
GROUP BY 1,2

ORDER BY 1,2