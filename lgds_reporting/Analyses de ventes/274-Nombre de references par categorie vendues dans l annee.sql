/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 

 -------- SQL -------- */

WITH category_translation AS (
    SELECT 
    CASE 
    WHEN product_category.name IN ('Animaux','Bazar','Entretien 5.5%','Entretien 20%','TVA10','Librairie','Presse') THEN 'Bazar'
    WHEN product_category.name IN ('Bien-être','Bien-être 20%','Hygiène 20%','Hygiène','Enfance 20%') THEN 'Hygiène'
    WHEN product_category.name IN ('Boucherie','Poissonnerie') THEN 'Viande/Poisson'
    WHEN product_category.name IN ('Cave','Vins','Vrac Cave') THEN 'Cave'
    WHEN product_category.name IN ('Épicerie Sucrée','Épicerie Sucrée 20%','Enfance') THEN 'Epicerie Sucrée'
    WHEN product_category.name IN ('Frais','Oeufs') THEN 'Frais'
    WHEN product_category.name IN ('Vrac Liquide Entretien','Vrac Entretien','Vrac Liquide Hygiène') THEN 'Vrac Maison'
    WHEN product_category.name IN ('Vrac Salés','Vrac Liquide Cuisine') THEN 'Vrac Salée'
    WHEN product_category.name IN ('Vrac Sucré','Vrac Sucré 20%') THEN 'Vrac Sucré'
    WHEN product_category.name IN ('F&L') THEN 'Fruit et légume'
    ELSE product_category.name
    END as mother_category_name
    , product_category.id AS child_categ_id
    FROM product_category
    WHERE product_category.name NOT IN ('Vrac Reconditionné','Consigne','Archive 5.5','Préco','Vrac Frais','viins','Boxe 5.5','Archive 20%','Archive 20','All','Vrac Liquide Boisson')
)
SELECT mother_category_name, count(distinct product_product.id) as nb_product
FROM product_category
JOIN product_template ON product_category.id = product_template.categ_id 
JOIN product_product ON product_product.product_tmpl_id = product_template.id
JOIN pos_order_line ON pos_order_line.product_id = product_product.id
JOIN category_translation ON category_translation.child_categ_id = product_template.categ_id 
WHERE product_product.active AND {{order_date}}
GROUP BY 1
ORDER BY 2 DESC