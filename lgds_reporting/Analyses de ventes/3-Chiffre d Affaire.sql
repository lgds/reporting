/* 
 -------- Description --------

[Date vente, category, periode vente]


 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 
{{ category }} -> product_category.name 

 -------- SQL -------- */

SELECT 
TO_CHAR(pos_order_line.create_date, 'YYYY-' || CASE {{ time_period }} WHEN 'month' THEN 'MM' WHEN 'week' THEN 'IW' WHEN 'day' THEN 'MM-dd' END) AS period
, SUM(pos_order_line.qty*pos_order_line.price_unit) AS "Chiffre d'affaire" 
FROM pos_order_line
JOIN product_product p ON p.id = pos_order_line.product_id
JOIN product_template pt ON p.product_tmpl_id= pt.id
JOIN product_category on product_category.id = pt.categ_id
WHERE {{ order_date }}
[[AND {{ category }}]]
GROUP BY 1 
ORDER BY 1