/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH stat_shift AS ( 
    SELECT partner_id, name, COUNT(DISTINCT CASE WHEN state = 'done' THEN id ELSE NULL END)/COUNT(DISTINCT CASE WHEN state IN ('done','open','excused') THEN id ELSE NULL END)::decimal(10,2)*100 as tx_presence
    FROM shift_registration
    WHERE date_begin BETWEEN now()-interval '6 months' AND now()
    AND state NOT IN ('draft','cancel','waiting')
    GROUP BY 1,2
)
SELECT DATE_TRUNC('month', pos_order.create_date) as mois, SUM(CASE WHEN tx_presence >= {{ pct_presence}} THEN amount_untaxed ELSE 0 END)/SUM(amount_untaxed)::decimal(10,2)*100  as vente
FROM stat_shift
JOIN pos_order ON pos_order.partner_id = stat_shift.partner_id
GROUP BY 1
ORDER BY 1,2