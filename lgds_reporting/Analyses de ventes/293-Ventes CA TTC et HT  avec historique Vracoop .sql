/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order.create_date 

 -------- SQL -------- */

WITH current_db as (
    SELECT
        DISTINCT ON (pos_order.id)
        pos_order.create_date,
        pos_order.amount_untaxed,
        SUM(pos_order_line.qty*pos_order_line.price_unit) OVER (PARTITION BY pos_order.id) AS amount_taxed,
        pos_order_line.qty AS quantity
    FROM pos_order
    JOIN pos_order_line ON pos_order_line.order_id = pos_order.id
    JOIN product_product ON product_product.id = pos_order_line.product_id
    JOIN product_template pt ON product_product.product_tmpl_id= pt.id
    JOIN product_category on product_category.id = pt.categ_id
    WHERE
    {{ order_date }}
), all_history AS (
    SELECT  
        DATE_TRUNC({{ time_granularity }}, create_date) AS order_date,
        SUM(amount_taxed) AS ca_ttc,
        SUM(amount_untaxed) AS ca_ht,
        SUM(quantity) as quantity_ordered
    FROM current_db
    GROUP BY 1
    
    UNION
    
    SELECT  
        DATE_TRUNC({{ time_granularity }}, pos_order_line.create_date) AS order_date,
        SUM(pos_order_line.price_subtotal_incl) AS ca_ttc,
        SUM(pos_order_line.price_subtotal) AS ca_ht,
        SUM(qty) as quantity_ordered
    FROM vracoop.pos_order_line
    JOIN vracoop.product_product ON product_product.id = pos_order_line.product_id
    JOIN vracoop.product_template pt ON product_product.product_tmpl_id= pt.id
    JOIN vracoop.product_category on product_category.id = pt.categ_id
    GROUP BY 1 
    ORDER BY 1
)
SELECT order_date, SUM(ca_ttc) as ca_ttc, SUM(ca_ht) as ca_ht, sum(quantity_ordered) AS quantity_ordered
FROM all_history
GROUP BY 1