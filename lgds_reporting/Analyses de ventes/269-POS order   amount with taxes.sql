/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

SELECT DISTINCT pos_order.*, SUM(pol.price_unit * pol.qty) OVER (PARTITION BY pos_order.id) AS amount_with_taxes
FROM pos_order
LEFT JOIN pos_order_line pol ON pol.order_id = pos_order.id