/* 
 -------- Description --------




 -------- Filters translation -------- 


 -------- SQL -------- */

WITH ol AS (
SELECT TO_CHAR(ol.create_date,'YYYY-MM-dd') AS order_date, order_id
, SUM(ol.qty*ol.price_unit/(1+t.amount/100)) AS turnover_ht
, SUM(ol.qty*ol.price_unit) AS turnover_ttc
FROM pos_order_line ol 
join account_tax_pos_order_line_rel tol ON ol.id = tol.pos_order_line_id
JOIN account_tax t ON t.id = tol.account_tax_id 
WHERE TO_CHAR(ol.create_date,'YYYY-MM-dd')  = '2020-11-21'
GROUP BY 1,2
)
SELECT TO_CHAR(o.create_date,'YYYY-MM-dd') 
, sum(o.amount_untaxed) as order_untax
, sum(turnover_ht) as ol_untax
, sum(turnover_ttc) as ol_tax
FROM pos_order  o 
JOIN ol on ol.order_id = o.id
GROUP BY 1
ORDER BY 1