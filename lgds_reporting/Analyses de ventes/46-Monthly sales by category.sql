/* 
 -------- Description --------




 -------- Filters translation -------- 

{{ order_date }} -> pos_order_line.create_date 
{{ categ_name }} -> product_category.name 
{{ product_name }} -> product_product.name_template 

 -------- SQL -------- */

SELECT  
    DATE_TRUNC({{ time_granularity }}, pos_order_line.create_date) AS order_date,
    product_category.name,
    SUM(pos_order_line.qty*pos_order_line.price_unit) AS turnover
FROM pos_order_line
JOIN product_product ON product_product.id = pos_order_line.product_id
JOIN product_template pt ON product_product.product_tmpl_id= pt.id
JOIN product_category on product_category.id = pt.categ_id
WHERE {{ order_date }}
[[AND {{ categ_name }} ]]
[[AND {{ product_name }} ]]
GROUP BY 1,2
ORDER BY 1,3 DESC