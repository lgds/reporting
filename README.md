# Reporting - Analyses de données

Ce dépôt git contient plusieurs choses :

- Un utilitaire en Python qui permet d'exporter/importer des questions Metabase dans des fichiers *.sql.
  
  _Voir le dossier [sync_metabase_sql/](./sync_metabase_sql/)_

- Les questions Metabase utilisées au supermarché Les Grains de Sel pour leurs analyses de données. Questions en fichiers `*.sql` exporté depuis l'utilitaire `sync_metabase_sql`.
  
  _Voir le dossier [lgds_reporting/](./lgds_reporting/)_

- Des scripts qui permettent de construire une base de donnée de reporting
  pour le supermarché Les Grains de Sel.
  
  Notamment pour anonymiser les données confidentielles dans Odoo,
  factoriser du code SQL utilisable dans Metabase,
  « brancher » plusieurs base de données entre elles afin de faire des requêtes agrégées entre elles,
  …
  
  _Voir le dossier [scripts/db/](./scripts/db/)_
