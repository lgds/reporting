------------------------------------------------------------------------
-- This script takes parameters as inputs:                            --
--                                                                    --
-- - odoo_host the host of the odoo database                          --
-- - odoo_port the port of the odoo database                          --
-- - odoo_dbname the database name of odoo                            --
-- - odoo_user the username of the odoo database                      --
-- - odoo_password the password of the odoo user                      --
-- - vracoop_password the password of the vracoop user                --
--                                                                    --
-- You can pass those parameters manually with `--set` param of psql. --
-- Our CI passes those parameters via the `\set` psql command.        --
-- (don't forget to add it in the load-pass.sh script of this repo)   --
--                                                                    --
-- ALso this script has to run as a DB SuperUser.                     --
------------------------------------------------------------------------
BEGIN;

---------------------------
-- Setup foreign servers --
---------------------------
CREATE EXTENSION IF NOT EXISTS postgres_fdw;

-- Odoo RO replica database (provided by Cooperatic)
CREATE SERVER IF NOT EXISTS "odoo" FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    host :'odoo_host',
    port :'odoo_port',
    dbname :'odoo_dbname',
    use_remote_estimate 'true'
);

CREATE USER MAPPING IF NOT EXISTS FOR PUBLIC SERVER "odoo" OPTIONS (
    USER :'odoo_user',
    PASSWORD :'odoo_password'
);

-- Historic Vracoop database (provided by Vracoop)
CREATE SERVER IF NOT EXISTS "vracoop" FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    host '127.0.0.1',
    dbname 'vracoop',
    port '5444',
    use_remote_estimate 'true'
);

CREATE USER MAPPING IF NOT EXISTS FOR PUBLIC SERVER "vracoop" OPTIONS (
    USER 'vracoop',
    PASSWORD :'vracoop_password'
);

COMMIT;
