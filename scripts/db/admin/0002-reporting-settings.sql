--------------------------------------------------------------------------------------------
-- Configure the 'reporting' role to limit queries to 240s (4 minutes)                    --
-- This is needed because metabase doesn't have a configuration to stop very long queries --
-- and thus we were having some never ending queries running forever on the PG database   --
--------------------------------------------------------------------------------------------
ALTER ROLE "reporting" SET statement_timeout = '240s';
