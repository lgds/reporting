BEGIN;

-----------------------------------------------------------------------
-- Set default access permissions for any subsequent object creation --
-----------------------------------------------------------------------
-- creation privilege (to create additional data tables and views)
ALTER DEFAULT PRIVILEGES GRANT CREATE, USAGE ON SCHEMAS TO "reporting";
ALTER DEFAULT PRIVILEGES GRANT SELECT ON TABLES TO "reporting";

-------------------------
-- Prepare all schemas --
-------------------------
-- Dedicated schema for odoo data
DROP SCHEMA IF EXISTS "odoo" CASCADE;
CREATE SCHEMA "odoo";

-- Dedicated schema for vracoop data
DROP SCHEMA IF EXISTS "vracoop" CASCADE;
CREATE SCHEMA "vracoop";

-- Explicit read privilege on tables and views for each schema
ALTER DEFAULT PRIVILEGES IN SCHEMA "odoo" GRANT
SELECT
    ON TABLES TO "reporting";
ALTER DEFAULT PRIVILEGES IN SCHEMA "vracoop" GRANT
SELECT
    ON TABLES TO "reporting";

----------------------------------
-- Drop existing foreign tables --
----------------------------------
DO $$
DECLARE
    tablenames TEXT;
BEGIN
    tablenames := STRING_AGG('"' || foreign_table_schema || '"."' || foreign_table_name || '"', ', ')
FROM
    information_schema.foreign_tables;
    IF tablenames IS NOT NULL THEN
    EXECUTE 'DROP FOREIGN TABLE ' || tablenames || ' CASCADE';
    END IF;
END;
$$;

----------------------------------------
-- Import tables from foreign servers --
----------------------------------------
-- odoo
IMPORT FOREIGN SCHEMA public
FROM
    SERVER "odoo" INTO "odoo";

-- vracoop
IMPORT FOREIGN SCHEMA public
FROM
    SERVER "vracoop" INTO "vracoop";

----------------------------------------------------------------------------------------
-- Analyze foreign tables                                                             --
-- ---                                                                                --
-- As documented in https://www.postgresql.org/docs/current/postgres-fdw.html         --
-- Running an `ANALYZE` query after importing foreign tables is a good way to update   --
-- the local statistics of those foreign tables. It will help the query planer to run --
-- better queries on foreign tables.                                                  --
----------------------------------------------------------------------------------------
DO $$
DECLARE
    tablenames TEXT;
BEGIN
    tablenames := STRING_AGG('"' || foreign_table_schema || '"."' || foreign_table_name || '"', ', ')
FROM
    information_schema.foreign_tables;
    EXECUTE 'ANALYZE ' || tablenames;
END;
$$;

COMMIT;
