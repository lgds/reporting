BEGIN;

----------------------------
-- Load pg anon extension --
----------------------------
ALTER DATABASE reporting SET session_preload_libraries='anon';

CREATE EXTENSION anon CASCADE;

SELECT anon.init();

COMMIT;

-- needs a reconnection
\c reporting

BEGIN;

-- Dynamic anon masking
SELECT anon.start_dynamic_masking(sourceschema := 'odoo');
SECURITY LABEL FOR anon ON ROLE reporting IS 'MASKED';

----------------
-- Anon rules --
----------------

-----------------------------------
-- Rules on the 'res_partner' table

-- coop id
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.barcode_base
  IS 'MASKED WITH FUNCTION abs(anon.random_int_between(barcode_base - 100, barcode_base + 100))';

SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.barcode
  IS 'MASKED WITH FUNCTION anon.partial(barcode,2,''xxxxxxxxx'',2)';

-- address
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.street
  IS 'MASKED WITH FUNCTION anon.partial(street,2,''xxxxxxxxx'',2)';

-- name (in format 'lastname, firstname')
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.name
  IS 'MASKED WITH FUNCTION (CASE WHEN NOT(is_company OR supplier) THEN UPPER(anon.fake_last_name()) || '', '' || anon.fake_first_name() ELSE name END)';
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.display_name
  IS 'MASKED WITH FUNCTION (CASE WHEN NOT(is_company OR supplier) THEN (abs(anon.random_int_between(barcode_base - 100, barcode_base + 100)) || '' - '' || anon.fake_last_name() || '', '' || anon.fake_first_name()) ELSE display_name END)';

-- phone and mobile
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.phone
  IS 'MASKED WITH FUNCTION (CASE WHEN phone = '''' THEN '''' ELSE anon.partial(phone,2,$$******$$,2) END)';
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.mobile
  IS 'MASKED WITH FUNCTION (CASE WHEN mobile = '''' THEN '''' ELSE anon.partial(mobile,2,$$******$$,2) END)';

-- email
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.email
  IS 'MASKED WITH FUNCTION anon.partial_email(email)';

-- birthdate
SECURITY LABEL FOR anon ON COLUMN odoo.res_partner.birthdate
  IS 'MASKED WITH FUNCTION (anon.random_date_between(birthdate::date - interval ''2 years'', birthdate::date + interval ''2 years'')::date)';

-----------------------------------
-- Rules on the 'res_partner_owned_share' table

SECURITY LABEL FOR anon ON COLUMN odoo.res_partner_owned_share.name
  IS 'MASKED WITH FUNCTION (anon.fake_last_name() || '', '' || anon.fake_first_name() || '' - Parts Coopérateur'') ';

-----------------------------------
-- Rules on the 'res_users' table

SECURITY LABEL FOR anon ON COLUMN odoo.res_users.login
  IS 'MASKED WITH FUNCTION anon.fake_last_name()';

SECURITY LABEL FOR anon ON COLUMN odoo.res_users.password_crypt
  IS 'MASKED WITH FUNCTION (''$pbkdf2-sha512$19000$ofQ.J4TQeg/hXOu913ovhQ$gonnuFiWAkvvnNFZxeBIelPb3anzbIVSUEoyw.BNLxUvyegBR61HBYPx8JoDiqvOJviJJwo91.9LhyWzc8Nt.Q'') ';

SECURITY LABEL FOR anon ON COLUMN odoo.res_users.signature
  IS 'MASKED WITH FUNCTION (''<p><br></p>'') ';

-----------------------------------
-- Rules on the 'shift_leave' table

SECURITY LABEL FOR anon ON COLUMN odoo.shift_leave.name
  IS 'MASKED WITH FUNCTION SUBSTRING(name FROM ''#"% - #"%'' FOR ''#'') || anon.fake_last_name() || '', '' || anon.fake_first_name()';

-----------------------------------
-- Rules on the 'shift_registration' table

SECURITY LABEL FOR anon ON COLUMN odoo.shift_registration.name
  IS 'MASKED WITH FUNCTION anon.fake_last_name() || '', '' || anon.fake_first_name()';

-----------------------------------
-- Rules on the 'shift_template_registration' table

SECURITY LABEL FOR anon ON COLUMN odoo.shift_template_registration.name
  IS 'MASKED WITH FUNCTION anon.fake_last_name() || '', '' || anon.fake_first_name()';

-----------------------------------
-- Rules on the 'ir_attachment' table

SECURITY LABEL FOR anon ON COLUMN odoo.ir_attachment.res_name
  IS 'MASKED WITH FUNCTION (CASE WHEN res_model = ''res.partner'' THEN anon.fake_last_name() || '', '' || anon.fake_first_name() ELSE res_name END)';

-----------------------------------
-- Rules on the 'mail_mail' table

SECURITY LABEL FOR anon ON COLUMN odoo.mail_mail.body_html
  IS 'MASKED WITH FUNCTION (CASE WHEN body_html = '''' THEN '''' ELSE anon.lorem_ipsum(2) END)';

-- email
SECURITY LABEL FOR anon ON COLUMN odoo.mail_mail.email_to
  IS 'MASKED WITH FUNCTION anon.partial_email(email_to)';

-----------------------------------
-- Rules on the 'mail_message' table

SECURITY LABEL FOR anon ON COLUMN odoo.mail_message.record_name
  IS 'MASKED WITH FUNCTION (CASE WHEN model = ''res.partner'' THEN anon.fake_last_name() || '', '' || anon.fake_first_name() ELSE record_name END)';

-----------------------------------
-- Rules on the 'mail_tracking_value' table

SECURITY LABEL FOR anon ON COLUMN odoo.mail_tracking_value.new_value_char
  IS 'MASKED WITH FUNCTION (CASE WHEN field = ''partner_id'' THEN anon.fake_last_name() || '', '' || anon.fake_first_name() ELSE new_value_char END)';

COMMIT;
