# Migrations automatiques pour une base de reporting basé sur Odoo

Ce dossier contient des scripts `*.sql` qui sont lancés automatiquement pour configurer une base de donnée de reporting par dessus un schéma de Odoo.

## `admin/` contient des scripts à lancer avec un utilisateur postgresql SUPERUSER

Les scripts permettent notamment de :

- de se connecter à la base RO de la production de Odoo LGDS ;
- d'ajouter des règles d'anonymisations avec l'extension [pg anonymizer](https://gitlab.com/dalibo/postgresql_anonymizer).

## `reporting/` contient des scripts à lancer avec l'utilisateur de reporting

Les scripts permettent de définir des vues SQL généralisés pour éviter de dupliquer des logiques SQL souvent utilisés dans nos questions Metabase.
